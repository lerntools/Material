Mit Lerntools (https://codeberg.org/org/lerntools/ , ausprobieren unter
https://www.lerntools.org ) versuchen wir
ehrenamtlich selbst freie Schulsoftware anzubieten. Lerntools besteht
aus mehrerenTeilen, die alle höchsten Anspruch an Datenschutz (im Sinne
davon möglichst wenig Daten zu erfassen) stellen.
- Ideensammlung: ähnlich Padlet
- Umfrage
- ABCD Quiz: ähnlich Kahoot

Bisher nicht auf Lerntools aktiviert, aber möglich zu benutzen:
- Pairs- eine Art Memory
- Projector: eine Möglichkeit für Schüler Bilder auf den Browser des
Lehrers zu übertragen

Lerntools setzt darauf, dass Schulen/Institutionen eigene Instanzen
gründen oder anmieten (z.B. bei Weingärtner-IT fertig konfiguriert) und
alles möglichst dezentral funktioniert.
Lerntools.org wird ohne Garantie als Plattform zum ausprobieren privat
betrieben.

Zu erreichen sind wir unter team@lerntools.org oder über mastodon (
bildung.social/@lerntools)
Es gibt auch mehrere Videos zu Lerntools: 
https://tube.schule.social/accounts/lerntools/video-channels